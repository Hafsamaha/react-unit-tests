import { render, screen } from '@testing-library/react';
import App from './App';

test('renders header text', () => {
  render(<App />);
  const headerText = screen.getByText(/Home/i); // Ändern Sie den Text hier entsprechend
  expect(headerText).toBeInTheDocument();
});
