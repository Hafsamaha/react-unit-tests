import { render, screen, act } from '@testing-library/react';
import { ThemeProvider } from '../contexts/ThemeContext';
import Footer from './Footer';
import React from 'react';

test('renders Footer with correct text', () => {
  render(
    <ThemeProvider>
      <Footer />
    </ThemeProvider>
  );

  // Überprüfen Sie, ob der Text im Footer vorhanden ist
  expect(screen.getByText(/© Rezept-App 2024/i)).toBeInTheDocument();
});

test('toggles theme from Footer', () => {
  render(
    <ThemeProvider>
      <Footer />
    </ThemeProvider>
  );

  // Stellen Sie sicher, dass der Button für das Umschalten des Themas vorhanden ist
  const button = screen.getByRole('button', { name: /Toggle Theme/i });

  act(() => {
    button.click();
  });

  // Überprüfen Sie den Zustand nach dem ersten Klick
  expect(screen.getByRole('button', { name: /Toggle Theme/i })).toBeInTheDocument();

  act(() => {
    button.click();
  });

  // Überprüfen Sie den Zustand nach dem zweiten Klick
  expect(screen.getByRole('button', { name: /Toggle Theme/i })).toBeInTheDocument();
});
