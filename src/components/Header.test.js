import { render, screen, act } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from '../contexts/ThemeContext';
import Header from './Header';
import React from 'react';

test('renders Header and toggles theme', () => {
  render(
    <ThemeProvider>
      <Router>
        <Header />
      </Router>
    </ThemeProvider>
  );

  const button = screen.getByRole('button');
  expect(screen.getByText('Home')).toBeInTheDocument();

  act(() => {
    button.click();
  });

  expect(screen.getByRole('button')).toBeInTheDocument(); // Überprüfen Sie den Zustand nach dem ersten Klick

  act(() => {
    button.click();
  });

  expect(screen.getByRole('button')).toBeInTheDocument(); // Überprüfen Sie den Zustand nach dem zweiten Klick
});
