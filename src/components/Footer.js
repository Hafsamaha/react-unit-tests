import React from 'react';
import { useTheme } from '../contexts/ThemeContext';

const Footer = () => {
  const { theme, toggleTheme } = useTheme();

  return (
    <footer className="bg-blue-500 text-white p-4 text-center mt-auto">
      <p>© Rezept-App 2024</p>
      <button onClick={toggleTheme}>Toggle Theme</button>
    </footer>
  );
};

export default Footer;
