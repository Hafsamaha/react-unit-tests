import React from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from '../contexts/ThemeContext';
import { Sun, Moon } from 'lucide-react';

const Header = () => {
    const { theme, toggleTheme } = useTheme();

    console.log('Header rendered with theme:', theme);

    return (
        <header className="bg-blue-500 text-white p-4 flex justify-between items-center">
            <nav>
                <Link to="/" className="text-xl font-bold">Home</Link>
            </nav>
            <button 
                onClick={() => {
                    console.log("Theme switch button clicked");
                    toggleTheme();
                }} 
                className="p-2 bg-gray-300 dark:bg-gray-700 rounded-full"
            >
                {theme === 'light' ? <Moon size={24} /> : <Sun size={24} />}
            </button>
        </header>
    );
};

export default Header;
