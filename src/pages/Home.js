import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/recipes')
      .then(response => response.json())
      .then(data => {
        setRecipes(data);
      })
      .catch(error => {
        console.error('Error fetching recipes:', error);
      });
  }, []);

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Rezepte</h1>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
        {recipes.map(recipe => (
          <div key={recipe.id} className="border p-4 rounded-lg shadow-lg">
            <Link to={`/recipe/${recipe.id}`}>
              <h2 className="text-xl font-bold mb-2">{recipe.name}</h2>
              <img src={recipe.image} alt={recipe.name} className="w-full h-40 object-cover mb-4" />
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Home;
