import { render, screen, act } from '@testing-library/react';
import { ThemeProvider, useTheme } from './ThemeContext';
import React from 'react';

test('should toggle theme', () => {
  const TestComponent = () => {
    const { theme, toggleTheme } = useTheme();
    return (
      <div>
        <span>{theme}</span>
        <button onClick={toggleTheme}>Toggle Theme</button>
      </div>
    );
  };

  render(
    <ThemeProvider>
      <TestComponent />
    </ThemeProvider>
  );

  const button = screen.getByRole('button', { name: /Toggle Theme/i });
  expect(screen.getByText('light')).toBeInTheDocument();

  act(() => {
    button.click();
  });

  expect(screen.getByText('dark')).toBeInTheDocument();

  act(() => {
    button.click();
  });

  expect(screen.getByText('light')).toBeInTheDocument();
});
